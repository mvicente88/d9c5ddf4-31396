const faker = require('faker');
const fs = require('fs');

function generateRandomUser() {
  const sharedAddress = {
    street: 'Santa Eulalia',
    state: 'Hospitalet de Llobregat',
    city: 'Barcelona',
    country: 'España',
    zip: '08902',
  };

  const useSharedInfo = faker.datatype.boolean();

  const user = {
    name: faker.name.findName(),
    email: faker.internet.email(),
    birthDate: faker.date.past().toLocaleDateString(),
    address: useSharedInfo ? sharedAddress : {
      street: faker.address.streetName(),
      state: faker.address.state(),
      city: faker.address.city(),
      country: faker.address.country(),
      zip: faker.address.zipCode(),
    },
  };

  return user;
}

function generateRandomUsers(count) {
  const users = [];

  for (let i = 0; i < count; i++) {
    users.push(generateRandomUser());
  }

  return users;
}

const numberOfUsers = 10;
const randomUsers = generateRandomUsers(numberOfUsers);

fs.writeFileSync('randomUsers.json', JSON.stringify(randomUsers, null, 2));
