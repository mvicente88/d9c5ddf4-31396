# codechallenge-NodeJS

# Users API
Welcome to the Users API project. This API allows you to manage and retrieve user data.

## Getting Started
These instructions will help you set up and run the project on your local machine for development and testing purposes.

## Prerequisites
Before running the project, make sure you have the following installed:

Node.js: https://nodejs.org/
MongoDB: https://www.mongodb.com/

## Installation
Clone the repository to your local machine:

git clone https://github.com/your-username/users-api.git
Navigate to the project directory:

cd users-api

Install the project dependencies:

npm install

## Configuration
Create a .env file in the project root directory with the following content, replacing the placeholders with your own values:

PORT=3000
DB_USER=your-mongodb-username
DB_PASSWORD=your-mongodb-password
DB_SERVER=your-mongodb-server
DB_NAME=your-mongodb-database

## Running the API
To start the API, run the following command:
npm start
The API should be accessible at http://localhost:3000.

## API Endpoints
GET /api/users: Get a list of all users.
POST /api/users: Create a new user.
GET /api/users/:userId: Get a specific user by ID.
PUT /api/users/:userId: Update a specific user by ID.
DELETE /api/users/:userId: Delete a specific user by ID.
You can test these endpoints using tools like Postman or by making HTTP requests.

## Running Tests
To run the automated tests for this project, you can use the following command:
npm test

## Built With
Node.js
Express.js
MongoDB
Mongoose
Authors
Your Name

## Hosting URL: 

https://d9c5ddf4-31396.web.app




