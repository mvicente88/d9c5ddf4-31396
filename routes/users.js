const express = require('express');
const router = express.Router();


const UserController = require('../controllers/userController');

// Routes
router.get('/', UserController.getAllUsers);
router.post('/', UserController.createUser);
router.post('/create-multiple', UserController.createUsers);// Random users Script
router.get('/:userId', UserController.getUserById);
router.put('/:userId', UserController.updateUserById);
router.delete('/:userId', UserController.deleteUserById);


module.exports = router;
