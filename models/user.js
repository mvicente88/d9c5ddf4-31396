const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    name: String,
    email: String,
    birthDate: String,
    address: { type: Schema.Types.ObjectId, ref: 'Address' }
});

module.exports = mongoose.model('User', userSchema);
