const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config();
const usersRouter = require('./routes/users');
const port = process.env.NODE_ENV === 'test' ? 3001 : 3000;
const admin = require("firebase-admin");
const serviceAccount = require("./.firebase/serviceAccountKey.json");


admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://d9c5ddf4-31396.web.app" 
});
const apiUrl = 'https://d9c5ddf4-31396.web.app';

// Database connection
const mongoDB =
  'mongodb+srv://' +
  process.env.DB_USER +
  ':' +
  process.env.DB_PASSWORD +
  '@' +
  process.env.DB_SERVER +
  '/' +
  process.env.DB_NAME +
  '?retryWrites=true&w=majority';

async function main() {
  await mongoose.connect(mongoDB);
}
main().catch((err) => console.log(err));

app.use(bodyParser.json());

// CORS
const corsOptions = {
  origin: 'https://d9c5ddf4-31396.web.app',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
};

app.use(cors(corsOptions));

app.use('/', usersRouter);

// Error handler
app.use((err, req, res, next) => {
  const status = err.statusCode || 500;
  const message = err.message || 'Internal Server Error';
  console.log(`Nueva solicitud a ${req.originalUrl}`);
  res.status(status).json({ message, stack: err.stack });
});

app.get('/', (req, res) => {
  res.send('Hello, World!');
});

const server = app.listen(port, () => {
  console.log(`Server listening on PORT ${port}`);
});

module.exports = { app, server };
