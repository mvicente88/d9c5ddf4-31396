const request = require('supertest');
const { app, server } = require('../app');
    



describe('Testing user API', () => {

    let response;
    beforeEach(async () => {
        response = (await request(app).get('/users').send());
        
    })
    
        describe('GET /', () => {
            it('Route works', async () => {
                expect(response.status).toBe(200),
                expect(response.headers['content-type']).toContain('json')       
            })

            it('The request returns an user array', async () => {
                expect(response.body).toBeInstanceOf(Array);
                
            })
        })

    describe('POST /users', () => {
        it('should create a new user', async () => {
            const newUser = {
                name: 'Marisa',
                email: 'marisa@example.com',
                birthDate: '13/08/1988',
                address: {
                    street: 'Badal',
                    state: 'Barcelona',
                    city: 'Barcelona',
                    country: 'España',
                    zip: '08900'
                },
            },
                response = await request(app).post('/users').send(newUser)
            expect(response.status).toBe(201);
            expect(response.body).toHaveProperty('_id');
            createdUserId = response.body._id;

        });
    });

    describe('GET /users/:userId', () => {
        it('should return a specific user', async () => {
            const response = await request(app).get(`/users/${createdUserId}`);
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('_id', createdUserId);
        });
    });
    
    describe('PUT /users/:userId', () => {
        it('should update a specific user', async () => {
            const updatedUserData = {
                name: 'Marisa Updated Name',
                email: 'MarisaUpdated@example.com',
            };
            const response = await request(app)
                .put(`/users/${createdUserId}`)
                .send(updatedUserData);
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('name', updatedUserData.name);
            expect(response.body).toHaveProperty('email', updatedUserData.email);
        });
    });
    
    describe('DELETE /users/:userId', () => {
        it('should delete a specific user', async () => {
            const response = await request(app).delete(`/users/${createdUserId}`);
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('_id', createdUserId);
        });
    });

    afterAll((done) => {
        server.close(() => {
            done();
        });
    });

})