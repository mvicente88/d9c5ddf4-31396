const express = require('express');
const User = require("../models/user");
const Address = require ("../models/address");
const { responses } = require('../swagger.json')

console.log("Petición recibida en userController");


exports.getAllUsers = async (req, res) => {
    try {
        const user = await User.find().populate('address');
        res.status(200).json(user);
    } catch (error) {
        console.error(error);
        res.status(500).json(responses['500']);
    }
};

exports.createUser = async (req, res) => {
    try {
        console.log('Received POST request at /api/users/createUsers');
        const user = new User(req.body);
        const existingAddress = await Address.findOne(req.body.address); // Existing address verification
        if (existingAddress) {
            user.address = existingAddress._id;
        } else {
            const newAddress = new Address(req.body.address);
            await newAddress.save();
            user.address = newAddress._id;
        }
        const newUser = await user.save();
        // This is a populate to show the commplete address in the server response
        const userWithFullAddress = await User.findById(newUser._id).populate('address');
        res.status(201).json(userWithFullAddress);
    } catch (error) {
        console.error(error);
        res.status(405).json(responses['405']);
    }
};

// This is to create random users with an script

exports.createUsers = async (req, res) => {
  try {
    const users = req.body;
    const newUsers = [];

    for (const user of users) {
      const addressData = user.address;
      const existingAddress = await Address.findOne({ street: addressData.street, city: addressData.city });

      if (existingAddress) {
        user.address = existingAddress._id;
      } else {
        const newAddress = new Address(addressData);
        await newAddress.save();
        user.address = newAddress._id;
      }

      const newUser = new User(user);
      await newUser.save();      
      const userWithFullAddress = await User.findById(newUser._id).populate('address');
      newUsers.push(userWithFullAddress);
    }

    res.status(201).json(newUsers);
  } catch (error) {
    console.error(error);
    res.status(405).json(responses['405']);
  }
};



exports.getUserById = async (req, res) => {
    try {
        const user = await User.findById(req.params.userId).select('name id');
        if (!user) {
            return res.status(404).json(responses['404']);
        }
        res.status(200).json(user);
    } catch (error) {
        console.error(error);
        res.status(400).json(responses['400']);
    }
};

exports.updateUserById = async (req, res) => {
    try {
        const userId = req.params.userId;
        const userToUpdate = await User.findById(userId);

        if (!userToUpdate) {
            return res.status(404).json(responses['404']);
        }
        if (req.body.name) {
            userToUpdate.name = req.body.name;
        }
        if (req.body.email) {
            userToUpdate.email = req.body.email;
        }
        if (req.body.birthDate) {
            userToUpdate.birthDate = req.body.birthDate;
        }
        if (req.body.address) {
            userToUpdate.address = req.body.address;
        }

        const updatedUser = await userToUpdate.save();
        res.status(200).json(updatedUser);
    } catch (error) {
        console.error(error);
        res.status(400).json(responses['400']);
    }
};


exports.deleteUserById = async (req, res) => {
    try {
        const deletedUser = await User.findByIdAndDelete(
            req.params.userId,
        ).select('id');
        if (!deletedUser) {
            return res.status(404).json(responses['404'])
        }
        res.status(200).json(deletedUser)
    } catch (error) {
        console.error(error);
        res.status(400).json(responses['400']);
    }
}



